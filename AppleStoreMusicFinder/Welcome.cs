﻿using System;
using System.Linq;
using System.Reflection;

namespace AppleStoreMusicFinder
{
    public static class Welcome
    {
        public static void Show()
        {
            var attributes = typeof(Program).GetTypeInfo().Assembly.GetCustomAttributes().ToList();
            Console.WriteLine($"Welcome to {(attributes[3] as AssemblyTitleAttribute).Title}! Version {(attributes[12] as AssemblyFileVersionAttribute).Version}\n{(attributes[8] as AssemblyCopyrightAttribute).Copyright}\n");
        }
    }
}
