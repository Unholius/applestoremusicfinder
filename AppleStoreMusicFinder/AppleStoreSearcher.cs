﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace AppleStoreMusicFinder
{
    public class AppleStoreSearcher: ISearcher
    {
        string searchArtistUrl = @"https://itunes.apple.com/search?term={user_query}&media=music&entity=musicArtist";
        readonly string searchAlbumstUrl = @"https://itunes.apple.com/lookup?id={artist_id}&entity=album";
        readonly string responseArtistFileName = "searchArtist.json";
        readonly string responseAlbumFileName = "searchAlbum.json";
        private readonly ICache cache;

        public AppleStoreSearcher(ICache CacheProvider)
        {
            cache = CacheProvider;
        }

        public void SearchArtists(string UserQuery)
        {
            var artists = new List<Artist>();
            artists.AddRange(cache.GetCachedArtists(UserQuery));
            if (artists.Count == 0)
            {
                Console.WriteLine("Ищем исполнителей на сервере:");
                using (WebClient client = new WebClient())
                    client.DownloadFile(searchArtistUrl.Replace("{user_query}", UserQuery.Replace(" ", "+")), responseArtistFileName);
                var artistJson = File.ReadAllText(responseArtistFileName);
                dynamic artistData = JsonConvert.DeserializeObject(artistJson);
                var resultCount = Convert.ToInt32($"{artistData["resultCount"]}");
                if (resultCount == 0)
                {
                    Console.WriteLine("Не найдено ни одного совпадения для исполнителя");
                    return;
                }
                else
                {

                    for (int i = 0; i < resultCount; i++)
                    {
                        artists.Add(new Artist
                        {
                            Id = Convert.ToInt64($"{artistData["results"][i]["artistId"]}"),
                            Name = $"{artistData["results"][i]["artistName"]}"
                        });
                    }
                    cache.CacheUserQuery(UserQuery, artists.ToArray());
                }
            }
            else
            {
                Console.WriteLine("Используем данные Исполнителей из кэша:");
            }

            if (artists.Count == 1)
            {
                Console.WriteLine($"{artists[0].Name}");
                SearchAlbums(artists[0].Id);
                return;
            }
            else
            {
                Console.WriteLine($"Введите номер Исполнителя для которого искать альбомы (0-{artists.Count - 1}): ");
                for (int i = 0; i < artists.Count; i++)
                {
                    Console.WriteLine($"{i}. {artists[i].Name}");
                }
                Console.Write(">");
                var userAnswer = Convert.ToInt32(Console.ReadLine());
                SearchAlbums(artists[userAnswer].Id);
            }
        }

        public void SearchAlbums(long ArtistId)
        {
            var albums = new List<Album>();
            albums.AddRange(cache.GetCachedAlbums(ArtistId));
            if (albums.Count == 0)
            {
                Console.WriteLine("Ищем альбомы на сервере:");
                using (WebClient client = new WebClient())
                    client.DownloadFile(searchAlbumstUrl.Replace("{artist_id}", $"{ArtistId}"), responseAlbumFileName);
                var albumtJson = File.ReadAllText(responseAlbumFileName);
                dynamic albumData = JsonConvert.DeserializeObject(albumtJson);
                var resultCount = Convert.ToInt32($"{albumData["resultCount"]}");
                if (resultCount == 0)
                {
                    Console.WriteLine("Не найдено ни одного совпадения");
                    return;
                }
                else
                {
                    for (int i = 1; i < resultCount; i++)
                    {
                        albums.Add(new Album {
                            Id = Convert.ToInt64(albumData["results"][i]["collectionId"]),
                            ArtistId = ArtistId,
                            Name = $"{albumData["results"][i]["collectionName"]}",
                            ReleaseDate = Convert.ToDateTime($"{albumData["results"][i]["releaseDate"]}")
                        });
                    }
                    cache.CacheArtistAlbums(ArtistId, albums.ToArray());
                }
            } else
            {
                Console.WriteLine("Используем данные альбомов из кэша:");
            }
            for (int i=0; i<albums.Count; i++)
                Console.WriteLine($"{i}. {albums[i].Name} ({albums[i].ReleaseDate:yyyy})");
        }
    }
}
