﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

namespace AppleStoreMusicFinder
{
    public class XmlCache : ICache
    {
        DataSet cacheStorage = new DataSet("Cache");
        readonly string fileName = "cache.xml";

        public XmlCache()
        {
             if (File.Exists(fileName)) cacheStorage.ReadXml(fileName, XmlReadMode.ReadSchema); else Create();
        }

        public void Create()
        {
            var dtUserQuery = new DataTable("UserQuery");
            dtUserQuery.Columns.AddRange(new[]
            {
                new DataColumn("Id", typeof(long)),
                new DataColumn("QueryText", typeof(string)),
                new DataColumn("Date", typeof(DateTime))
            });
            dtUserQuery.Columns["Id"].AutoIncrement = true;
            dtUserQuery.Columns["Id"].Unique = true;
            dtUserQuery.Columns["Id"].AutoIncrementSeed = 1L;
            dtUserQuery.Columns["Id"].AutoIncrementStep = 1L;
            dtUserQuery.PrimaryKey = new[] { dtUserQuery.Columns["Id"] };

            var dtQueryMatches = new DataTable("QueryMatches");
            dtQueryMatches.Columns.AddRange(new[]
            {
                new DataColumn("UserQueryId", typeof(long)),
                new DataColumn("ArtistId", typeof(long))
            });
            dtQueryMatches.PrimaryKey = new[] { dtQueryMatches.Columns["UserQueryId"], dtQueryMatches.Columns["ArtistId"] };

            var dtArtist = new DataTable("Artist");
            dtArtist.Columns.AddRange(new[]
            {
                new DataColumn("Id", typeof(long)),
                new DataColumn("ArtistName", typeof(string))
            });
            dtArtist.PrimaryKey = new[] { dtArtist.Columns["Id"] };

            var dtAlbum = new DataTable("Album");
            dtAlbum.Columns.AddRange(new[]
            {
                new DataColumn("Id", typeof(long)),
                new DataColumn("ArtistId", typeof(long)),
                new DataColumn("AlbumName", typeof(string)),
                new DataColumn("ReleaseDate", typeof(DateTime))
            });
            dtAlbum.PrimaryKey = new[] { dtAlbum.Columns["Id"] };

            cacheStorage.Tables.AddRange(new[] { dtUserQuery, dtQueryMatches, dtArtist, dtAlbum });

            dtQueryMatches.ParentRelations.AddRange(new[] {
                new DataRelation("fkUserQueryMatch", dtUserQuery.Columns["Id"], dtQueryMatches.Columns["UserQueryId"]),
                new DataRelation("fkArtistMatch", dtArtist.Columns["Id"], dtQueryMatches.Columns["ArtistId"]),
            });

            dtAlbum.ParentRelations.Add(new DataRelation("fkArtist", dtArtist.Columns["Id"], dtAlbum.Columns["ArtistId"]));

            cacheStorage.AcceptChanges();
            cacheStorage.WriteXml("cache.xml", XmlWriteMode.WriteSchema);
        }

        public void CacheUserQuery(string UserQuery, Artist[] artists)
        {
            var rows = cacheStorage.Tables["UserQuery"].Select($"QueryText LIKE '{UserQuery}'");
            if (rows.Length > 0) return;

            var rowUserQuery = cacheStorage.Tables["UserQuery"].NewRow();
            rowUserQuery["QueryText"] = UserQuery;
            rowUserQuery["Date"] = DateTime.Now;
            cacheStorage.Tables["UserQuery"].Rows.Add(rowUserQuery);

            foreach (var artist in artists)
            {
                var existArtists = cacheStorage.Tables["Artist"].Select($"Id={artist.Id}");
                if (existArtists.Length == 0)
                {
                    var rowArtist = cacheStorage.Tables["Artist"].NewRow();
                    rowArtist["Id"] = artist.Id;
                    rowArtist["ArtistName"] = artist.Name;
                    cacheStorage.Tables["Artist"].Rows.Add(rowArtist);
                    cacheStorage.Tables["QueryMatches"].Rows.Add(rowUserQuery["Id"], artist.Id);
                } else
                {
                    foreach (var rowArtist in existArtists)
                        if (cacheStorage.Tables["QueryMatches"].Select($"UserQueryId={rowUserQuery["Id"]} AND ArtistId={rowArtist["Id"]}").Length == 0)
                            cacheStorage.Tables["QueryMatches"].Rows.Add(rowUserQuery["Id"], rowArtist["Id"]);
                }
            }
            cacheStorage.AcceptChanges();
            cacheStorage.WriteXml(fileName, XmlWriteMode.WriteSchema);
        }

        public void CacheArtistAlbums(long ArtistId, Album[] albums)
        {
            foreach (var album in albums)
            {
                if (cacheStorage.Tables["Album"].Select($"Id='{album.Id}'").Length == 0)
                {
                    cacheStorage.Tables["Album"].Rows.Add(album.Id, ArtistId, album.Name, album.ReleaseDate);
                }
            }
            cacheStorage.AcceptChanges();
            cacheStorage.WriteXml(fileName, XmlWriteMode.WriteSchema);
        }

        public Artist[] GetCachedArtists(string UserQuery)
        {
            var result = new List<Artist>();
            var rowsUserQuery = cacheStorage.Tables["UserQuery"].Select($"QueryText LIKE '{UserQuery}'");
            foreach (var row in rowsUserQuery)
            {
                var rowsMatches = cacheStorage.Tables["QueryMatches"].Select($"UserQueryId={row["Id"]}");
                foreach (var rowArtistMatch in rowsMatches)
                {
                    var rowArtist = cacheStorage.Tables["Artist"].Select($"Id={rowArtistMatch["ArtistId"]}");
                    if (rowArtist.Length == 1) result.Add(new Artist
                    {
                        Id = (long)rowArtist[0]["Id"],
                        Name = $"{rowArtist[0]["ArtistName"]}"
                    });
                }
            }
            return result.ToArray();
        }

        public Album[] GetCachedAlbums(long ArtistId)
        {
            var result = new List<Album>();
            var rowsAlbum = cacheStorage.Tables["Album"].Select($"ArtistId={ArtistId}");
            foreach (var rowAlbum in rowsAlbum)
            {
                result.Add(
                    new Album
                    {
                        Id = (long)rowAlbum["Id"],
                        ArtistId = (long)rowAlbum["ArtistId"],
                        Name = $"{rowAlbum["AlbumName"]}",
                        ReleaseDate = rowAlbum["ReleaseDate"] == DBNull.Value ? null : (DateTime?)rowAlbum["ReleaseDate"]
                    });
            }
            return result.ToArray();
        }
    }
}
