﻿namespace AppleStoreMusicFinder
{
    interface ISearcher
    {
        void SearchArtists(string UserQuery);
        void SearchAlbums(long ArtistId);
    }
}