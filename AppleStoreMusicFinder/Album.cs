﻿using System;

namespace AppleStoreMusicFinder
{
    public class Album
    {
        public long Id;
        public long ArtistId;
        public string Name;
        public DateTime? ReleaseDate;
    }
}
