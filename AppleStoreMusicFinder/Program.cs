﻿using System;
using System.Linq;

namespace AppleStoreMusicFinder
{
    class Program
    {
        //TODO: create abstract output-input results system
        //TODO: create factory for searchers
        //TODO: create factory for cache providers
        //TODO: some checks for correct inputs
        //TODO: cache entries expire date
        //TODO: write some comments
        //TODO: use async await search methods
        //TODO: some results are very strange...
        //TODO: make system menu: search, clear cache, choose search provider, exit, etc...

        /// <summary>
        /// This is program's entry point
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Welcome.Show();
            var cache = new XmlCache();
            var searcher = new AppleStoreSearcher(cache);
            var userInput = "";
            if (args.Length == 0)
            {
                Console.Write("Введите имя Исполнителя: ");
                userInput = Console.ReadLine();
            } else
            {
                userInput = args.Aggregate((i, j) => i + "+" + j); // but this is more readable: string.Join("+", args);
                Console.WriteLine($"Исполнитель: {userInput}");
            }
            if (!string.IsNullOrWhiteSpace(userInput))
                searcher.SearchArtists(userInput);

            Console.WriteLine("Нажмите любую клавишу для выхода...");
            Console.ReadKey();
        }
    }
}
