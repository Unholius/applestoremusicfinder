﻿namespace AppleStoreMusicFinder
{
    public interface ICache
    {
        void Create();
        void CacheUserQuery(string UserQuery, Artist[] artists);
        void CacheArtistAlbums(long ArtistId, Album[] albums);

        Artist[] GetCachedArtists(string UserQuery);

        Album[] GetCachedAlbums(long ArtistId);
    }
}
